package l1


import java.io.File

import entity.{ByteR, ParamsL1}
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.io.Source

object L1 {

  def generation(params:ParamsL1): Unit = {

    if (params.m > 255) {
      println("The module must be less than 256!")
      System.exit(1)
    }

    var x = params.x0
    var t = 0
    var T = 0
    var countEven = 0
    var countUnits = 0
    println("x(0) = " + x + " = " + new ByteR(x.toByte).strBits)
    if (x % 2 == 0)
      countEven += 1
    countUnits += new ByteR(x.toByte).countUnits

    (1 until params.m).iterator.takeWhile(_ => params.x0 != (params.a * x + params.c) % params.m).foreach(i => {
      x = (params.a * x + params.c) % params.m
      countUnits += new ByteR(x.toByte).countUnits
      if (x % 2 == 0)
        countEven += 1
      t = i
      println("x(" + i + ") = " + x + " = " + new ByteR(x.toByte).strBits)
    })

    T = t * 8 + new ByteR(x.toByte).indexLastBit
    println("T(в битах) = " + T)
    println("Четных = " + countEven)
    println("Нечетных = " + (t + 1 - countEven))
    println("Колич. единиц = " + countUnits)
    println("Колич. нулей = " + (T - countUnits))
  }

  def main(args: Array[String]): Unit = {

    implicit val formats = DefaultFormats
    val params = new File("src/main/resources/paramsL1.json").getAbsolutePath
    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL1]
    generation(json)
  }

}
