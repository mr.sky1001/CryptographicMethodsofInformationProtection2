package l6

import java.io.File

import entity.{ParamsL5, ParamsL6}
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, _}
import dao.Dao._
import l4.L5
import scala.io.Source
//val messByteR = ArrayByteToArrayByteR(message.getBytes())
//var CRC: String = ""
//implicit val formats = DefaultFormats
//val params = new File("src/main/resources/paramsL5.json").getAbsolutePath
//var file: String = new String
//Source.fromFile(params).foreach(s => file += s)
//val json = parse(file).extract[ParamsL5]
//messByteR.foreach { byte =>
//CRC += L5.calcCRC(byte.strBits, json)
//}
//if (Integer.parseInt(CRC) % p < 2) {
//2
//}
//else if (Integer.parseInt(CRC) % p >= p) {
//p - 1
//}
//else {
//Integer.parseInt(CRC) % p
//}

object L6 {
  def calcHEX(message: String, p: Int): Int = {
    implicit val formats = DefaultFormats
    val params = new File("src/main/resources/paramsL5.json").getAbsolutePath
    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL5]
    val CRC: String = L5.calcCRC(message, json)
    val result = Integer.parseInt(CRC, 2) % p
    if (result < 2) {
      2
    }
    else if (result >= p) {
      p - 1
    }
    else {
      result
    }

  }

  def DigitalSignature(message: String, params: ParamsL6) {

    val h = calcHEX(message, params.p)
    val y = powMod(params.g, params.x, params.p)
    val k = randomScala(params.p - 1, 2, params.p) % (params.p - 1)
    val r = powMod(params.g, k, params.p)
    val u = if ((h - params.x * r) % (params.p - 1) < 0) (params.p - 1) + (h - params.x * r) % (params.p - 1) else (h - params.x * r) % (params.p - 1)
    val s = if ((inverse(k, params.p - 1) * u) % (params.p - 1) < 0) (params.p - 1) + (inverse(k, params.p - 1) * u) % (params.p - 1) else (inverse(k, params.p - 1) * u) % (params.p - 1)
    println("(M, r, s): ")
    println("M =  " + message)
    println("r =  " + r)
    println("s =  " + s)
    println("k =  " + k)
    println("inverse(k) =  " + inverse(k, params.p - 1))

    val yrrs = (powMod(y, r, params.p) * powMod(r, s, params.p)) % params.p
    val gh = powMod(params.g, h, params.p)
    println("y^r * r^s = " + yrrs)
    println("g^h mod p =  " + gh)
  }

  //10011101
  def main(args: Array[String]): Unit = {

    try {
      val inp = readLine("Input message: ")
      val endlindex = if (inp.length < 8) inp.length else 8
      val inpString = inp.substring(0, endlindex)

      implicit val formats = DefaultFormats
      val params = new File("src/main/resources/paramsL6.json").getAbsolutePath
      var file: String = new String
      Source.fromFile(params).foreach(s => file += s)
      val json = parse(file).extract[ParamsL6]
      DigitalSignature(inpString, json)
    }
    catch {
      case e: NumberFormatException => println("Please input message in format (xxxxxxxx) x={0,1}!")
    }
  }

}
