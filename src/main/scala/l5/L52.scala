//package l5
//
////import com.sun.java.util.jar.pack.Package.File
//import java.io.File
//
//import entity.ParamsL5
//import org.json4s.jackson.JsonMethods._
//import org.json4s.{DefaultFormats, _}
//
//import scala.io.Source
//
//object L52 {
//
//
//
//  def calcHEX(inpInt:Int, params: ParamsL5): Unit = {
//    val  A = "10011101"
//    val B = "101"
//
//    val  c = Integer.parseInt(A, 2) % Integer.parseInt(B, 2)
//    println("inp = " + inpInt)
//    println("inpR = " + Integer.toBinaryString(inpInt))
//    println("param = " + Integer.parseInt(params.polynomial, 2))
//    println("a = " + Integer.parseInt(A, 2))
//    println("b = " + Integer.parseInt(B, 2))
//    println("c = " + c)
//
//  }
//
//  def calcColison(inpBytes:Array[Byte], params: ParamsL5): Unit = {
//
//  }
//
//  def main(args: Array[String]): Unit = {
//
//    //val inp = new File("src/main/resources/inpL5.txt").getAbsolutePath
//    //val inpfileLinux = new File("src/main/resources/inpL5.txt").getAbsolutePath
//    val inp    = readLine("Input message: ")
//    try {
//      val inpBytes = inp.toInt
//
//    }
//    finally {
//      val inpBytes = inp.
//    }
//    implicit val formats = DefaultFormats
//    val params =new File("src/main/resources/paramsL5.json").getAbsolutePath
//    //val paramsLinux = new File("src/main/resources/paramsL5.json").getAbsolutePath
//    var file: String = new String
//    Source.fromFile(params).foreach(s => file += s)
//    val json = parse(file).extract[ParamsL5]
//    calcHEX(inpBytes, json )
//
//   // calcColison(inpBytes, json)
//
//  }
//
//}


//import com.sun.java.util.jar.pack.Package.File
import java.io.File

import dao.Dao._
import dao._
import entity.ParamsL5
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, _}

import scala.collection.mutable.ListBuffer
import scala.io.Source

//  "polynomial":"101001"
object L52 {


  def calcCRC(inp: String, params: ParamsL5): Int = {
    println("<----------------" + Integer.parseInt(inp, 2) + "----------------------->")
    println("inp = " + inp)
    println("polynomial= " + params.polynomial)

    val N = params.polynomial.length
    var inpM = inp
    (0 until N).foreach { _ =>
      inpM += "0"
    }

    var interimI = 0
    var interimS = ""

    var startIndex = 0
    var endlIndex = params.polynomial.length
    val first = inpM.substring(startIndex, endlIndex)
    startIndex = endlIndex
    interimI = Integer.parseInt(first, 2) ^ Integer.parseInt(params.polynomial, 2)
    interimS = Integer.toBinaryString(interimI)

    if (interimS.length < params.polynomial.length) {
      endlIndex += params.polynomial.length - interimS.length
      if (endlIndex >= inpM.length) {
        endlIndex = inpM.length
        interimS += inpM.substring(startIndex, endlIndex)
        interimI = Integer.parseInt(interimS, 2)
        println("CRC = " + interimS)
        println("CRCI = " + interimI)
        println("<------------------------------------------>")
        println()
        return interimI
      }
      interimS += inpM.substring(startIndex, endlIndex)
      interimI = Integer.parseInt(interimS, 2)
      startIndex = endlIndex
    }

    while (interimS.length > params.polynomial.length - 1) {
      interimI = interimI ^ Integer.parseInt(params.polynomial, 2)
      interimS = Integer.toBinaryString(interimI)

      if (interimS.length < params.polynomial.length) {
        endlIndex += params.polynomial.length - interimS.length
        if (endlIndex > inpM.length) {
          endlIndex = inpM.length
          interimS += inpM.substring(startIndex, endlIndex)
          interimI = Integer.parseInt(interimS, 2)
          println("CRC = " + interimS)
          println("CRCI = " + interimI)
          println("<------------------------------------------>")
          println()
          return interimI
        }
        interimS += inpM.substring(startIndex, endlIndex)
        interimI = Integer.parseInt(interimS, 2)
        startIndex = endlIndex
      }
    }
    interimI
  }

  def calcColision(params: ParamsL5): Int = {
    var count = 0
    var list = new ListBuffer[Int]
    (1 to 255).foreach { n =>
      val temp = calcCRC(Integer.toBinaryString(n), params)
      if(!list.contains(temp)){
        list += temp
      }
      else{
        count += 1
      }
    }
    count
  }

  def main(args: Array[String]): Unit = {
    try {
      val inp = readLine("Input message: ")
      val endlindex = if (inp.length < 8) inp.length else 8
      val inpString = inp.substring(0, endlindex)
      implicit val formats = DefaultFormats
      val params = new File("src/main/resources/paramsL5.json").getAbsolutePath
      var file: String = new String
      Source.fromFile(params).foreach(s => file += s)
      val json = parse(file).extract[ParamsL5]
      calcCRC(inpString, json)
      println("Count collision: " + calcColision(json))
    }
    catch {
      case e: NumberFormatException => println("Please input message in format (xxxxxxxx) x={0,1}!")
    }
  }

}

/////////////////////////////111111111111111111111111111/////////////////////////////
//package l4
//
////import com.sun.java.util.jar.pack.Package.File
//import java.io.File
//
//import dao.Dao._
//import dao._
//import entity.ParamsL5
//import org.json4s.jackson.JsonMethods._
//import org.json4s.{DefaultFormats, _}
//
//import scala.collection.mutable.ListBuffer
//import scala.io.Source
//
////  "polynomial":"101001"
//object L5 {
//
//
//  def calcHEX(inp: String, params: ParamsL5): Unit = {
//    println("polynomial = " + params.polynomial)
//    println("polynomialInt = " + Integer.parseInt(params.polynomial, 2))
//
//    val N = params.polynomial.length
//    println("N = " + N)
//    var inpM = inp
//    (0 until N).foreach { _ =>
//      inpM += "0"
//    }
//    println("inp = " + inp)
//    println("inpM = " + inpM)
//    println("inpMInt = " + Integer.parseInt(inpM, 2))
//
//    ////
//    var temp = ""
//
//    var interimI = 0
//    //временный
//    var interimS = "" //временный
//
//    var startIndex = 0
//    var endlIndex = params.polynomial.length
//    val first = inpM.substring(startIndex, endlIndex)
//    startIndex = endlIndex
//    println("first = " + first)
//    interimI = Integer.parseInt(first, 2) ^ Integer.parseInt(params.polynomial, 2)
//    println("interimI = " + interimI)
//    interimS = Integer.toBinaryString(interimI)
//
//    if (interimS.length < params.polynomial.length) {
//      endlIndex += params.polynomial.length - interimS.length
//      if (endlIndex >= inpM.length) {
//        endlIndex = inpM.length
//        interimS += inpM.substring(startIndex, endlIndex)
//        interimI = Integer.parseInt(interimS, 2)
//        println("CRC = " + interimS)
//        println("CRCI = " + interimI)
//        return
//      }
//      interimS += inpM.substring(startIndex, endlIndex)
//      interimI = Integer.parseInt(interimS, 2)
//      startIndex = endlIndex
//    }
//    println("interimSNew = " + interimS)
//    println("interimINEW = " + interimI)
//
//    while (interimS.length > params.polynomial.length - 1) {
//      interimI = interimI ^ Integer.parseInt(params.polynomial, 2)
//      interimS = Integer.toBinaryString(interimI)
//      println("interimI = " + interimI)
//      println("interimS = " + interimS)
//
//      if (interimS.length < params.polynomial.length) {
//        endlIndex += params.polynomial.length - interimS.length
//        if (endlIndex > inpM.length) {
//          endlIndex = inpM.length
//          interimS += inpM.substring(startIndex, endlIndex)
//          interimI = Integer.parseInt(interimS, 2)
//          println("CRC = " + interimS)
//          println("CRCI = " + interimI)
//          return
//        }
//        interimS += inpM.substring(startIndex, endlIndex)
//        interimI = Integer.parseInt(interimS, 2)
//        startIndex = endlIndex
//      }
//      println("interimSNew = " + interimS)
//      println("interimINEW = " + interimI)
//    }
//
//
//    //    println("CRC = " + Integer.toBinaryString(Integer.parseInt(inpM, 2) % Integer.parseInt(params.polynomial, 2)))
//    //    println("CRCInt = " + Integer.parseInt(inpM, 2) % Integer.parseInt(params.polynomial, 2))
//  }
//
//  def calcColison(inpBytes: Array[Byte], params: ParamsL5): Unit = {
//
//  }
//
//  def main(args: Array[String]): Unit = {
//    try {
//      val inp = readLine("Input message: ")
//
//
//      val endlindex = if (inp.length < 8) inp.length else 8
//      val inpString = inp.substring(0, endlindex)
//      implicit val formats = DefaultFormats
//      val params = new File("src/main/resources/paramsL5.json").getAbsolutePath
//      var file: String = new String
//      Source.fromFile(params).foreach(s => file += s)
//      val json = parse(file).extract[ParamsL5]
//      calcHEX(inpString, json)
//      calcColison(inpBytes, json)
//    }
//    catch {
//      case e: NumberFormatException => println("Please input message in format (xxxxxxxx) x={0,1}!")
//    }
//  }
//
//}
