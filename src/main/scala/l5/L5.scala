package l4

//import com.sun.java.util.jar.pack.Package.File
import java.io.File

import dao.Dao._
import dao._
import entity.ParamsL5
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, _}

import scala.collection.mutable.ListBuffer
import scala.io.Source

//  "polynomial":"101001"
object L5 {


  def calcCRC(inp: String, params: ParamsL5): String = {
    println("<----------------" + Integer.parseInt(inp, 2) + "----------------------->")
    println("inp = " + inp)
    println("polynomial= " + params.polynomial)

    val N = params.polynomial.length
    var inpM = inp
    (0 until N).foreach { _ =>
      inpM += "0"
    }

    var interimI = 0
    var interimS = ""

    var startIndex = 0
    var endlIndex = params.polynomial.length
    val first = inpM.substring(startIndex, endlIndex)
    startIndex = endlIndex
    interimI = Integer.parseInt(first, 2) ^ Integer.parseInt(params.polynomial, 2)
    interimS = Integer.toBinaryString(interimI)

    if (interimS.length < params.polynomial.length) {
      endlIndex += params.polynomial.length - interimS.length
      if (endlIndex >= inpM.length) {
        endlIndex = inpM.length
        interimS += inpM.substring(startIndex, endlIndex)
        interimI = Integer.parseInt(interimS, 2)
        println("CRC = " + interimS)
        println("CRCI = " + interimI)
        println("<------------------------------------------>")
        println()
        return interimS
      }
      interimS += inpM.substring(startIndex, endlIndex)
      interimI = Integer.parseInt(interimS, 2)
      startIndex = endlIndex
    }

    while (interimS.length > params.polynomial.length - 1) {
      interimI = interimI ^ Integer.parseInt(params.polynomial, 2)
      interimS = Integer.toBinaryString(interimI)

      if (interimS.length < params.polynomial.length) {
        endlIndex += params.polynomial.length - interimS.length
        if (endlIndex > inpM.length) {
          endlIndex = inpM.length
          interimS += inpM.substring(startIndex, endlIndex)
          interimI = Integer.parseInt(interimS, 2)
          println("CRC = " + interimS)
          println("CRCI = " + interimI)
          println("<------------------------------------------>")
          println()
          return interimS
        }
        interimS += inpM.substring(startIndex, endlIndex)
        interimI = Integer.parseInt(interimS, 2)
        startIndex = endlIndex
      }
    }
    interimS
  }

  def calcColision(params: ParamsL5): Int = {
    var count = 0
    var list = new ListBuffer[String]
    (1 to 255).foreach { n =>
      val temp = calcCRC(Integer.toBinaryString(n), params)
      if(!list.contains(temp)){
        list += temp
      }
      else{
        count += 1
      }
    }
    count
  }

  def main(args: Array[String]): Unit = {
    try {
      val inp = readLine("Input message: ")
      val endlindex = if (inp.length < 8) inp.length else 8
      val inpString = inp.substring(0, endlindex)
      implicit val formats = DefaultFormats
      val params = new File("src/main/resources/paramsL5.json").getAbsolutePath
      var file: String = new String
      Source.fromFile(params).foreach(s => file += s)
      val json = parse(file).extract[ParamsL5]
      calcCRC(inpString, json)
      println("Count collision: " + calcColision(json))
    }
    catch {
      case e: NumberFormatException => println("Please input message in format (xxxxxxxx) x={0,1}!")
    }
  }

}
