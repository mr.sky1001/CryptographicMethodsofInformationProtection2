package entity

import java.util

case class ParamsL1(a: Int, c: Int, m: Int, x0: Int)

case class ParamsL2(arrayInit: String, f1: Boolean, f2: Boolean, f3: Boolean, f4: Boolean, f5: Boolean, f6: Boolean, f7: Boolean, f8: Boolean)

case class ParamsL4(p: Int, q: Int)

case class ParamsL5(polynomial: String)

case class ParamsL6(p: Int, g: Int, x: Int)

case class ParamsL7(e: Int, N: Int, C: Int)

class ByteR(byte: Byte) {
  var strBits: String = new String
  var bits: util.BitSet = util.BitSet.valueOf(Array(byte))
  var countUnits: Int = 0
  var countNulls: Int = 0
  var indexLastBit: Int = 0
  (0 until 8).foreach(i =>
    if (this.bits.get(i)) {
      this.strBits += "1"
      this.indexLastBit = (i - 7) % 8 * (-1)
      this.countUnits += 1
    }
    else {
      this.countNulls += 1
      this.strBits += "0"
    })

}
