package l4

//import com.sun.java.util.jar.pack.Package.File
import java.io.File

import dao.Dao._
import dao._
import entity.ParamsL4
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, _}

import scala.collection.mutable.ListBuffer
import scala.io.Source

object L4 {

  def getBlocks(n: Int, strListNum: String): ListBuffer[Int] = {
    //размер модуля
    var sizeN = 0
    var del = 1
    while (n / del > 0) {
      del *= 10
      sizeN += 1
    }

    var arrBloks = new ListBuffer[Int]()
    var r = 0
    while (r < strListNum.length) {
      while (r + sizeN > strListNum.length) {
        sizeN -= 1
      }

      val num = strListNum.substring(r, r + sizeN).toInt
      if (num >= n) {
        arrBloks += strListNum.substring(r, r + sizeN - 1).toInt
        r = r + sizeN - 1
      }
      else {
        arrBloks += num
        r = r + sizeN
      }

    }
    println("M = " + strListNum)
    println("Mblocs = " + arrBloks)
    arrBloks
  }

  def calcListE(factNumbers: List[Int], p: Int, q: Int): ListBuffer[Int] = {
    var list = new ListBuffer[Int]
    factNumbers.iterator.foreach(j => if (gcd(j, (p - 1) * (q - 1)) == 1) list += j)
    list
  }

  def calcEandD(p: Int, q: Int): List[Int] = {

    var i = 1
    while (factorize((p - 1) * (q - 1) * i + 1).length < 4) {
      i += 1
    }
    val factNumbers = factorize((p - 1) * (q - 1) * i + 1)

    val listE = calcListE(factNumbers, p, q)
    val e1 = listE.head
    val e2 = listE(1)
    val e3 = listE(2)
    val d1 = inverse(e1, (p - 1) * (q - 1))
    val d2 = inverse(e2, (p - 1) * (q - 1))
    val d3 = inverse(e3, (p - 1) * (q - 1))

    println("list keys:")
    println("open: e = " + e1 + "; n = " + p * q + "; close: d = " + d1)
    println("open: e = " + e2 + "; n = " + p * q + "; close: d = " + d2)
    println("open: e = " + e3 + "; n = " + p * q + "; close: d = " + d3)
    //    var d = 1
    //    factNumbers.iterator.foreach(j => if (j != e1) d *= j)
    List(e1, d1)
  }

  def encryption(m: String, params: ParamsL4, outfile: String): Unit = {

    val n = params.p * params.q
    println("-----Task2-----")
    val ed = calcEandD(params.p, params.q)
    val listNumbers = stringToNumber(m)
    var strListNum = new String

    listNumbers.iterator.foreach(j => strListNum += j.toString)
    println("-----Task1-----")
    val arrBloks = getBlocks(n, strListNum)
    val listC = arrBloks.map(b => powMod(b, ed.head, n))
    println("C = " + listC)

    writeString(outfile, listC)

  }

  def decryption(params: ParamsL4, outfile: String, inpfile: String): Unit = {
    val n = params.p * params.q
    println("-----Task2-----")
    val ed = calcEandD(params.p, params.q)
    val strListNum = Dao.readString(outfile)
    println(strListNum)
    println("-----Task1-----")
    val arrBloks = getBlocks(n, strListNum)
    val listM = arrBloks.map(b => powMod(b, ed.last, n))
    println("M = " + listM)
    val listWords = numbersToString(listM)

    writeString(inpfile, listWords)
  }

  def main(args: Array[String]): Unit = {
    val inp = new File("src/main/resources/inpL4.txt").getAbsolutePath
    val inp2 = new File("src/main/resources/inpL4Out.txt").getAbsolutePath
    val m = Dao.readString(inp)
    val params = new File("src/main/resources/paramsL4.json").getAbsolutePath

    implicit val formats = DefaultFormats
    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL4]

    encryption(m, json, inp)
    decryption(json, inp, inp)
    //ответ в inp
  }

}
