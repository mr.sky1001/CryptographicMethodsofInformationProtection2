package l2

import java.io.File
import java.util

import dao._
import entity.{ByteR, ParamsL2}
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable.ListBuffer
import scala.io.Source

object L2 {
  def generation(params: ParamsL2, size: Int): Array[util.BitSet] = {
    var arrBits = Dao.stringBitsToBitSet(params.arrayInit)
    var gamma = new String
    var arrBegin = new String
    val arrOut = new Array[util.BitSet](size)
    (0 until size).iterator.foreach(i => arrOut(i) = new util.BitSet())
    var T = 0
    var ind = 1
    var ind2 = 0
    var countEven = 0
    println("Начальное: " + new ByteR(arrBits.toByteArray.head).strBits)
    while (ind < size) {
      val a0 = (params.f1 & arrBits.get(7)) ^ (params.f2 & arrBits.get(6)) ^ (params.f3 & arrBits.get(5)) ^ (params.f4 & arrBits.get(4)) ^
        (params.f5 & arrBits.get(3)) ^ (params.f6 & arrBits.get(2)) ^ (params.f7 & arrBits.get(1)) ^ (params.f8 & arrBits.get(0))

      arrOut(ind2).set(ind % 8, arrBits.get(7))
      arrBits.set(7, arrBits.get(6))
      arrBits.set(6, arrBits.get(5))
      arrBits.set(5, arrBits.get(4))
      arrBits.set(4, arrBits.get(3))
      arrBits.set(3, arrBits.get(2))
      arrBits.set(2, arrBits.get(1))
      arrBits.set(1, arrBits.get(0))
      arrBits.set(0, a0)
      if (arrOut(ind2).get(ind % 8)) {gamma+='1'} else {gamma+='0'}
      println("a0 = " + (if (a0) "1" else "0") + " z(" + ind + ") = " + new ByteR(arrBits.toByteArray.head).strBits + " -->  " + gamma.last)
      if (ind % 8 == 0) {
        if(ind2 == 0){
          arrBegin = gamma
        }
        if (arrOut(ind2).get(ind % 8)) {
          println("Нечетное.")
        }
        else {
          countEven += 1
          println("Четное.")
        }
      if (gamma.substring(ind-8,ind) == arrBegin && ind2 != 0) {
        T = ind2
        ind = size
      }
        ind2 += 1
      }
      ind += 1
    }
    println("Гамма: " + gamma.substring(0, gamma.length-8))
    println()
    println("T = " + T * 8)
    println("Четных: " + countEven)
    println("Нечетных: " + (T - countEven))
    println("Колич. единиц: " + gamma.substring(0, gamma.length-8).count(g => g=='1'))
    println("Колич. нулей: " +  gamma.substring(0, gamma.length-8).count(g => g=='0'))
    arrOut
  }

  def main(args: Array[String]): Unit = {

    implicit val formats = DefaultFormats
    val params = new File("src/main/resources/paramsL2.json").getAbsolutePath

    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL2]

    generation(json, 255)
  }

}
