package l7

//import com.sun.java.util.jar.pack.Package.File
import java.io.File

import dao.Dao._
import dao._
import entity.{ParamsL4, ParamsL7}
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, _}

import scala.collection.mutable.ListBuffer
import scala.io.Source
import dao.Dao._

object L7 {

  def decryption(params: ParamsL7): Unit = {
    println("N = " + params.N)
    println("e = " + params.e)
    println("C = " + params.C)
    println()
    println("n = pq;")
    val pq = factorize(params.N)
    println("n = " + pq.head + "*" + pq.last)
    println()
    println("ed  = 1 mod (p-1)(q-1)")
    println(params.e + "*d = 1 mod " + (pq.head - 1) + "*" + (pq.last - 1))
    val d = if(inverse(params.e, (pq.head - 1) * (pq.last - 1)) < 0) (pq.head - 1) * (pq.last - 1) + inverse(params.e, (pq.head - 1) * (pq.last - 1)) else inverse(params.e, (pq.head - 1) * (pq.last - 1))
    println("d = " + d + " mod " + (pq.head - 1) * (pq.last - 1))
    println("d = " + d)
    println()
    println("НОД(e, d) = " + gcd(params.e, d))
    println("M = C^d mod n")
    println("M = " + powMod(params.C, d, params.N))
  }

  def main(args: Array[String]): Unit = {
    val params = new File("src/main/resources/paramsL7.json").getAbsolutePath
    implicit val formats = DefaultFormats
    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL7]
    decryption(json)

  }

}
