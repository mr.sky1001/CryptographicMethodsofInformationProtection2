package l3

import java.io.File
import java.util

import dao._
import entity._
import l2.L2
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable.ListBuffer
import scala.io.Source

object L3 {
//  def generation2(params: ParamsL2, size: Int): Array[util.BitSet] = {
//    var arrBits = Dao.stringBitsToBitSet(params.arrayInit)
//    val arrOut = new Array[util.BitSet](size)
//    (0 until size).iterator.foreach(i => arrOut(i) = new util.BitSet())
//    var ind = 1
//    var ind2 = 0
//    while (ind < size) {
//      val a0 = (params.f1 & arrBits.get(7)) ^ (params.f2 & arrBits.get(6)) ^ (params.f3 & arrBits.get(5)) ^ (params.f4 & arrBits.get(4)) ^
//        (params.f5 & arrBits.get(3)) ^ (params.f6 & arrBits.get(2)) ^ (params.f7 & arrBits.get(1)) ^ (params.f8 & arrBits.get(0))
//
//      arrOut(ind2).set(ind % 8, arrBits.get(7))
//      arrBits.set(1, arrBits.get(0));
//      arrBits.set(2, arrBits.get(1))
//      arrBits.set(3, arrBits.get(2));
//      arrBits.set(4, arrBits.get(3))
//      arrBits.set(5, arrBits.get(4));
//      arrBits.set(6, arrBits.get(5))
//      arrBits.set(7, arrBits.get(6));
//      arrBits.set(0, a0)
//
//      if (arrOut(ind2).length % 8 == 0) {
//        ind2 += 1
//      }
//      ind += 1
//    }
//    arrOut
//  }
//
//  def generation(params: ParamsL2, size: Int): Array[util.BitSet] = {
//
//    val arrBits: Array[util.BitSet] = new Array[util.BitSet](size)
//    (0 until size).iterator.foreach(i => arrBits(i) = new util.BitSet())
//    arrBits(0) = Dao.stringBitsToBitSet(params.arrayInit)
//    var t = 0
//    var T = 0
//    var countEven = 0
//    var countUnits = 0
//
//    if (arrBits(0).toByteArray.head % 2 == 0)
//      countEven += 1
//    println("z(0) = " + new ByteR(arrBits(0).toByteArray.head).strBits)
//
//    var a8: Boolean = false
//    var ind = 1
//    while (ind < size) {
//      a8 = (params.f1 & arrBits(ind - 1).get(7)) ^ (params.f2 & arrBits(ind - 1).get(6)) ^ (params.f3 & arrBits(ind - 1).get(5)) ^ (params.f4 & arrBits(ind - 1).get(4)) ^
//        (params.f5 & arrBits(ind - 1).get(3)) ^ (params.f6 & arrBits(ind - 1).get(2)) ^ (params.f7 & arrBits(ind - 1).get(1)) ^ (params.f8 & arrBits(ind - 1).get(0))
//
//      arrBits(ind).set(0, arrBits(ind - 1).get(1))
//      arrBits(ind).set(1, arrBits(ind - 1).get(2))
//      arrBits(ind).set(2, arrBits(ind - 1).get(3))
//      arrBits(ind).set(3, arrBits(ind - 1).get(4))
//      arrBits(ind).set(4, arrBits(ind - 1).get(5))
//      arrBits(ind).set(5, arrBits(ind - 1).get(6))
//      arrBits(ind).set(6, arrBits(ind - 1).get(7))
//      arrBits(ind).set(7, a8)
//
//      if (t == 0) {
//        println("z(" + ind + ") = " + new ByteR(arrBits(ind).toByteArray.head).strBits + " a8 = " + a8)
//        countUnits += new ByteR(arrBits(ind).toByteArray.head).countUnits
//
//      }
//      if (arrBits(ind).toByteArray.head % 2 == 0 && t == 0)
//        countEven += 1
//
//      if (arrBits(ind) == Dao.stringBitsToBitSet(params.arrayInit) && t == 0) {
//        t = ind
//
//      }
//      ind += 1
//    }
//    T = t * 8 + new ByteR(arrBits(t).toByteArray.head).indexLastBit
//    println("T = " + T)
//    println("Четных = " + countEven)
//    println("Нечетных = " + (t + 1 - countEven))
//    println("Колич. единиц = " + countUnits)
//    println("Колич. нулей = " + (T - countUnits))
//    arrBits
//  }

  def encryption(inpBytes: Array[Byte], params: ParamsL2, outfile: String): Unit = {
    println("Encryption... ")
    val size = inpBytes.length
    val inpBits: Array[util.BitSet] = new Array[util.BitSet](size)
    (0 until size).iterator.foreach(i => inpBits(i) = util.BitSet.valueOf(Array(inpBytes(i))))
    val genBits = L2.generation(params, size)
    val outBits: Array[util.BitSet] = new Array[util.BitSet](size)
    (0 until size).iterator.foreach(i => outBits(i) = new util.BitSet())

    (0 until size).iterator.foreach { i =>
      (0 until 8).foreach(j =>
        outBits(i).set(j, inpBits(i).get(j) ^ genBits(i).get(j))
      )
    }
    val outBytes: Array[Byte] = new Array[Byte](size)
    for (i <- 0 until size) {
      if (outBits(i).length() > 0)
        outBytes(i) = outBits(i).toByteArray.head
    }
    println("Write to file...")
    Dao.writeBytes(outfile, outBytes)
  }

//  def decryption(inpBytes: Array[Byte], params: ParamsL2, outfile: String): Unit = {
//    println("Decryption............................................................................................................................................ ")
//    val size = inpBytes.length
//    val inpBits: Array[util.BitSet] = new Array[util.BitSet](size)
//    (0 until size).iterator.foreach(i => inpBits(i) = util.BitSet.valueOf(Array(inpBytes(i))))
//    val genBits = L2.generation(params, size)
//    val outBits: Array[util.BitSet] = new Array[util.BitSet](size)
//    (0 until size).iterator.foreach(i => outBits(i) = new util.BitSet())
//    (0 until size).iterator.foreach(i =>
//      (0 until 8).foreach(j =>
//        outBits(i).set(j, inpBits(i).get(j) ^ genBits(i).get(j))
//      )
//    )
//    val outBytes: Array[Byte] = new Array[Byte](size)
//    (0 until size).iterator.foreach { i =>
//      if (outBits(i).length() > 0)
//        outBytes(i) = outBits(i).toByteArray.head
//    }
//    println("Write to file...")
//    Dao.writeBytes(outfile, outBytes)
//  }

  def main(args: Array[String]): Unit = {

    val inpfile = readLine("Input: ")
    //val inpfile = new File("src/main/resources/inpL3.txt").getAbsolutePath
    val inpBytes = Dao.readBytes(new File(inpfile))
    implicit val formats = DefaultFormats
    val params = new File("src/main/resources/paramsL3.json").getAbsolutePath
    var file: String = new String
    Source.fromFile(params).foreach(s => file += s)
    val json = parse(file).extract[ParamsL2]

    encryption(inpBytes, json, inpfile)
//    val inpBytesDec = Dao.readBytes(new File(inpfile))
//    decryption(inpBytesDec, json, inpfile)


  }

}
